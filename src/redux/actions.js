const changeContentType = (contentType) => ({
    type: 'CHANGE_CONTENT_TYPE',
    contentType: contentType    
});

const addContent = (value, updateContent) => ({
    type: 'ADD_CONTENT',
    content: value,
    updateContent: updateContent
})

export { changeContentType, addContent }