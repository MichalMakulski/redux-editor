import { createStore, combineReducers } from 'redux';

const INITIAL_STATE = {
    contentType: 'paragraph',
    userInput: {
        content: '', 
        updateContent: false
    }
}

const store = createStore(combineReducers({contentType, userInput}));

function contentType (state = INITIAL_STATE.contentType, action) {
    const {type, contentType} = action;

    if (type === 'CHANGE_CONTENT_TYPE') {
        return contentType;
    }
    
    return state;
}

function userInput (state = INITIAL_STATE.userInput, action) {
    const {type, content, updateContent} = action;

    if (type === 'ADD_CONTENT') {
        return {...state, content, updateContent};
    }
    
    return state;
}

export { store };