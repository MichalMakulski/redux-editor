import { store } from './redux/store';
import { changeContentType, addContent } from './redux/actions';
import { global } from './styles';

const input = document.getElementById('text');

document.addEventListener('change', handleContentTypeChange, false);
input.addEventListener('keyup', handleContent, false);

store.subscribe(render);

function handleContentTypeChange (ev) {
    const target = ev.target;
    const headerRadio = document.getElementById('header');
    const paragraphRadio = document.getElementById('paragraph');
    const isRadio = target === headerRadio || target === paragraphRadio;
    
    if (isRadio) {
        store.dispatch(changeContentType(target.id));
    }
}

function handleContent (ev) {
    const updateContent = ev.key === 'Enter';
    let value = ev.target.value;
    
    if (updateContent) {
        value = value.slice(0, -1);    
    }
    
    store.dispatch(addContent(value, updateContent));
}

function render () {
    const {userInput, contentType} = store.getState();
    const setContentType = contentType === 'header' ? 'h2' : 'p';
    const elt = document.createElement(setContentType);
    const contentHolder = document.getElementById('content');
    
    elt.textContent = userInput.content;
    input.value = userInput.content;
    
    if (userInput.updateContent) {
        contentHolder.appendChild(elt);
        console.log(store.getState())
        store.dispatch(addContent('', false));
    }
}